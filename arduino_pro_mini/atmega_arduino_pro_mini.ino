/* ATtiny85 as an I2C Slave            BroHogan                           1/12/11
 * Example of ATtiny I2C slave receiving and sending data to an Arduino master.
 * Gets data from master, adds 10 to it and sends it back.
 * SETUP:
 * ATtiny Pin 1 = (RESET) N/U                      ATtiny Pin 2 = (D3) N/U
 * ATtiny Pin 3 = (D4) to LED1                     ATtiny Pin 4 = GND
 * ATtiny Pin 5 = I2C SDA on DS1621  & GPIO        ATtiny Pin 6 = (D1) to LED2
 * ATtiny Pin 7 = I2C SCK on DS1621  & GPIO        ATtiny Pin 8 = VCC (2.7-5.5V)
 * NOTE! - It's very important to use pullups on the SDA & SCL lines!
 * Current Rx & Tx buffers set at 32 bytes - see usiTwiSlave.h
 * Credit and thanks to Don Blake for his usiTwiSlave code. 
 * More on TinyWireS usage - see TinyWireS.h
 */


#include <Wire.h>

#define I2C_SLAVE_ADDR  0x26            // i2c slave address (38)
#define LED1_PIN         13              // ATtiny Pin 3
#define PWM_PIN         5              // ATtiny Pin 6

unsigned long previousMicros = 0;        // will store last time pulse was updated
int pwmPulseWidth = 0;
int pulseState = LOW;
  
void setup(){
  pinMode(LED1_PIN,OUTPUT);             // for general DEBUG use
  pinMode(PWM_PIN,OUTPUT);             // for verification
  //Blink(LED1_PIN,5);                    // show it's alive
  Wire.begin(I2C_SLAVE_ADDR);      // init I2C Slave mode
  Wire.onReceive(receiveEvent);
}


void loop(){
  unsigned long currentMicros = micros();
  if (((currentMicros - previousMicros >= pwmPulseWidth) and (pulseState == 1 )) || pwmPulseWidth==0 ) {
    // save the last time you 
    previousMicros = currentMicros;
    digitalWrite(PWM_PIN, LOW);
    pulseState=LOW;
  }
  if ((currentMicros - previousMicros >= ( 20000 - pwmPulseWidth )) and (pulseState == 0 ) ) {
    // save the last time you 
    previousMicros = currentMicros;
    digitalWrite(PWM_PIN, HIGH);
    pulseState=HIGH;
  }
}

void receiveEvent(int howMany) {
  byte byteRcvd = 0;
//  while (1 < Wire.available()) { // loop through all but the last
//    char c = Wire.read(); // receive byte as a character
//  }
  byteRcvd = Wire.read();    // receive byte as an integer
  pwmPulseWidth = byteRcvd * 10;
}
